.PHONY: install-kind
install-kind:
	brew install kind

.PHONY: install-pyroscope
install-pyroscope:
	helm repo add pyroscope-io https://pyroscope-io.github.io/helm-chart
	helm install pyroscope pyroscope-io/pyroscope --set service.type=NodePort

