package cache

import (
	"errors"
	"sync"
)

// This is not optimal implementation, but it's good enough for now.
// To proper use https://github.com/facebookresearch/faiss

type Sample[T any] struct {
	Hash  T
	Value []float32
}

type HashCache[T any] struct {
	lock    *sync.Mutex
	maxSize int
	samples []Sample[T]
	differ  func(*T, *T) int
	maxDiff int
}

func New[T any](maxSize int, maxDiff int, differ func(*T, *T) int) *HashCache[T] {
	return &HashCache[T]{
		maxSize: maxSize,
		lock:    &sync.Mutex{},
		samples: make([]Sample[T], 0, maxSize),
		maxDiff: maxDiff,
		differ:  differ,
	}
}

func (c *HashCache[T]) FindSimilar(hash *T) (Sample[T], error) {
	for _, s := range c.samples {
		if c.differ(&s.Hash, hash) < c.maxDiff {
			return s, nil
		}
	}

	return Sample[T]{}, errors.New("not found")
}

func (c *HashCache[T]) Add(hash T, value []float32) {
	c.lock.Lock()
	defer c.lock.Unlock()
	c.samples = append(c.samples, Sample[T]{Hash: hash, Value: value})

	if len(c.samples) > c.maxSize {
		c.samples = c.samples[1:]
	}
}
