package perceptual_hash

import (
	"bytes"
	"encoding/binary"
	"image/jpeg"

	"github.com/corona10/goimagehash"
)

type ImageHash struct {
	hash *goimagehash.ImageHash
}

func Calculate(image []byte) (*ImageHash, error) {

	img, err := jpeg.Decode(bytes.NewReader(image))
	if err != nil {
		return nil, err
	}

	hash, err := goimagehash.AverageHash(img)
	if err != nil {
		return nil, err
	}

	return &ImageHash{hash: hash}, nil
}

func Deserialize(val []byte) ImageHash {
	num := binary.LittleEndian.Uint64(val)
	return ImageHash{
		hash: goimagehash.NewImageHash(num, goimagehash.AHash),
	}
}

func (h *ImageHash) Serialize() []byte {
	buf := new(bytes.Buffer)
	_ = binary.Write(buf, binary.LittleEndian, h.hash.GetHash())

	return buf.Bytes()
}

func Differ(a, b *ImageHash) int {
	res, _ := a.hash.Distance(b.hash)
	return res
}
