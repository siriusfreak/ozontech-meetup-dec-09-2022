package main

import (
	"context"
	"fmt"
	"net"
	"sync"

	"github.com/caarlos0/env/v6"
	"github.com/pyroscope-io/client/pyroscope"
	srv "gitlab.com/siriusfreak/ozontech-meetup-dec-09-2022/cache/pkg/api"
	"gitlab.com/siriusfreak/ozontech-meetup-dec-09-2022/cache/srv/cache"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	log "github.com/sirupsen/logrus"
)

type config struct {
	Port    int `env:"PORT" envDefault:"8080"`
	MaxSize int `env:"MAX_SIZE" envDefault:"1000"`
	MaxDiff int `env:"MAX_DIFF" envDefault:"10"`

	PyroscopeHost string `env:"PYROSCOPE_HOST" envDefault:"localhost"`
	PyroscopePort int    `env:"PYROSCOPE_PORT" envDefault:"4040"`
}

func grpcPyroscope(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {

	wg := sync.WaitGroup{}

	wg.Add(1)
	pyroscope.TagWrapper(context.Background(),
		pyroscope.Labels("method", info.FullMethod), func(ctx context.Context) {
			resp, err = handler(ctx, req)
			wg.Done()
		})

	wg.Wait()
	return handler(ctx, req)
}

func main() {

	c := config{}
	err := env.Parse(&c)
	if err != nil {
		log.Fatal(err)
	}

	address := fmt.Sprintf("http://%s:%d", c.PyroscopeHost, c.PyroscopePort)
	_, err = pyroscope.Start(pyroscope.Config{
		Logger:          pyroscope.StandardLogger,
		ApplicationName: "cache",
		ServerAddress:   address,
	})
	if err != nil {
		log.Fatal(err)
	}

	address = fmt.Sprintf(":%d", c.Port)
	log.Printf("Starting cache at %s", address)
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	var opts []grpc.ServerOption
	opts = append(opts, grpc.UnaryInterceptor(grpcPyroscope))
	grpcServer := grpc.NewServer(opts...)
	srv.RegisterCacheServer(grpcServer, cache.Init(c.MaxSize, c.MaxDiff))
	reflection.Register(grpcServer)
	grpcServer.Serve(lis)
}
