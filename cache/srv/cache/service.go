package cache

import (
	"context"
	"errors"
	"log"

	"gitlab.com/siriusfreak/ozontech-meetup-dec-09-2022/cache/lib/cache"
	ph "gitlab.com/siriusfreak/ozontech-meetup-dec-09-2022/cache/lib/perceptual-hash"
	srv "gitlab.com/siriusfreak/ozontech-meetup-dec-09-2022/cache/pkg/api"
)

type PerceptualHashService struct {
	srv.UnimplementedCacheServer

	cache *cache.HashCache[ph.ImageHash]
}

func Init(maxSize int, maxDiff int) *PerceptualHashService {
	return &PerceptualHashService{
		cache: cache.New[ph.ImageHash](maxSize, maxDiff, ph.Differ),
	}
}

func (s *PerceptualHashService) Check(ctx context.Context, in *srv.CheckRequest) (*srv.CheckResponse, error) {
	hash, err := ph.Calculate(in.Image)
	if err != nil {
		return nil, err
	}

	val, err := s.cache.FindSimilar(hash)
	if err != nil {
		return &srv.CheckResponse{
			Hash:  hash.Serialize(),
			Score: nil,
		}, nil
	}

	return &srv.CheckResponse{
		Hash:  val.Hash.Serialize(),
		Score: val.Value,
	}, nil
}

func (s *PerceptualHashService) Store(ctx context.Context, in *srv.StoreRequest) (*srv.StoreResponse, error) {
	log.Printf("storing hash %v", in.Hash)

	if in.Hash == nil {
		return nil, errors.New("no hash provided")
	}

	s.cache.Add(ph.Deserialize(in.Hash), in.Score)
	return &srv.StoreResponse{}, nil
}
