package client

import (
	"context"

	srv "gitlab.com/siriusfreak/ozontech-meetup-dec-09-2022/cache/pkg/api"
	"google.golang.org/grpc"
)

type Cache struct {
	conn   *grpc.ClientConn
	client srv.CacheClient
}

func New(endpoint string, opts ...grpc.DialOption) (*Cache, error) {
	conn, err := grpc.Dial(endpoint, opts...)
	if err != nil {
		return nil, err
	}

	cache := &Cache{
		conn:   conn,
		client: srv.NewCacheClient(conn),
	}

	return cache, nil
}

func (c *Cache) Check(ctx context.Context, image []byte) ([]byte, []float32, error) {
	resp, err := c.client.Check(ctx, &srv.CheckRequest{
		Image: image,
	})

	return resp.GetHash(), resp.GetScore(), err
}

func (c *Cache) Store(ctx context.Context, hash []byte, score []float32) error {
	_, err := c.client.Store(ctx, &srv.StoreRequest{
		Hash:  hash,
		Score: score,
	})

	return err
}
