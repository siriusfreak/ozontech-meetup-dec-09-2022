package resizer

import (
	"context"
	"io"
	"log"
	"math/rand"
	"net/http"
	"time"

	srv "gitlab.com/siriusfreak/ozontech-meetup-dec-09-2022/resizer/pkg/api"
)

type CacheClient interface {
	Check(ctx context.Context, image []byte) ([]byte, []float32, error)
	Store(ctx context.Context, hash []byte, score []float32) error
}

type Service struct {
	srv.UnimplementedResizerServer

	cache CacheClient
}

func Init(cache CacheClient) *Service {
	return &Service{
		cache: cache,
	}
}

func downloadImage(url string) ([]byte, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	return io.ReadAll(response.Body)
}

func (s *Service) Process(ctx context.Context, in *srv.ProcessRequest) (*srv.ProcessResponse, error) {
	img, err := downloadImage(in.GetUrl())

	if err != nil {
		log.Printf("failed to download image: %v", err)
		return nil, err
	}

	hash, scores, err := s.cache.Check(ctx, img)
	if hash == nil {
		log.Printf("failed to check image: %v", err)
		return nil, err
	}

	if scores != nil {
		log.Printf("found similar image with hash %v", hash)
		return &srv.ProcessResponse{
			Scores: scores,
			Hashed: true,
		}, nil
	}

	time.Sleep(200 * time.Millisecond)
	scores = make([]float32, 10)
	for i := range scores {
		scores[i] = rand.Float32()
	}

	err = s.cache.Store(ctx, hash, scores)
	if err != nil {
		log.Printf("failed to store image: %v", err)
		return nil, err
	}

	return &srv.ProcessResponse{
		Scores: scores,
		Hashed: false,
	}, nil

}
