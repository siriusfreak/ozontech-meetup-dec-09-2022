package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"github.com/caarlos0/env/v6"
	"github.com/pyroscope-io/client/pyroscope"
	"gitlab.com/siriusfreak/ozontech-meetup-dec-09-2022/resizer/lib/client"
	srv "gitlab.com/siriusfreak/ozontech-meetup-dec-09-2022/resizer/pkg/api"
	"gitlab.com/siriusfreak/ozontech-meetup-dec-09-2022/resizer/srv/resizer"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/reflection"
)

type config struct {
	Port      int    `env:"PORT" envDefault:"8080"`
	CacheHost string `env:"CACHE_HOST" envDefault:"localhost"`
	CachePort int    `env:"CACHE_PORT" envDefault:"8080"`

	PyroscopeHost string `env:"PYROSCOPE_HOST" envDefault:"localhost"`
	PyroscopePort int    `env:"PYROSCOPE_PORT" envDefault:"4040"`
}

func grpcPyroscope(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	pyroscope.TagWrapper(context.Background(),
		pyroscope.Labels("method", info.FullMethod), func(ctx context.Context) {
			resp, err = handler(ctx, req)
		})

	return handler(ctx, req)
}

func main() {

	c := config{}
	err := env.Parse(&c)
	if err != nil {
		log.Fatal(err)
	}

	address := fmt.Sprintf("http://%s:%d", c.PyroscopeHost, c.PyroscopePort)
	_, err = pyroscope.Start(pyroscope.Config{
		Logger:          pyroscope.StandardLogger,
		ApplicationName: "resizer",
		ServerAddress:   address,
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Connecting to cache at %s:%d", c.CacheHost, c.CachePort)
	cacheClient, err := client.New(fmt.Sprintf("%s:%d", c.CacheHost, c.CachePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		log.Fatal(err)
	}

	address = fmt.Sprintf(":%d", c.Port)
	log.Printf("Starting resizer at %s", address)
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	opts = append(opts, grpc.UnaryInterceptor(grpcPyroscope))
	grpcServer := grpc.NewServer(opts...)
	srv.RegisterResizerServer(grpcServer, resizer.Init(cacheClient))
	reflection.Register(grpcServer)
	grpcServer.Serve(lis)
}
